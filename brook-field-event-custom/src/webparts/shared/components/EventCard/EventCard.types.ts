import { ISubscribe } from './../../services/CalendarService/ISubscribe';
import { ICalendarEvent } from "../../../shared/services/CalendarService";
import { IWebPartContext } from '@microsoft/sp-webpart-base';

export interface IEventCardProps {
  isEditMode: boolean;
  event: ICalendarEvent;
  subscribers?: ISubscribe[];
  isNarrow: boolean;
  context?: IWebPartContext;
}

export interface IEventCardState {
  auxSubscribe?: boolean;
  hideDialog: boolean | true;
}
