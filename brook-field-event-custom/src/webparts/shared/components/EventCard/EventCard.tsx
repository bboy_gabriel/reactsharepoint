import { Guid } from "@microsoft/sp-core-library";
import * as strings from 'EventCustomWebPartStrings';
import * as ICS from "ics-js";
import * as moment from "moment";
import { ActionButton, Button, DocumentCard, DocumentCardType, FocusZone, css } from "office-ui-fabric-react";
import * as React from "react";
import { IEventCardProps, IEventCardState } from ".";
import { DateBox, DateBoxSize } from "../DateBox";
import styles from "./EventCard.module.scss";
import { Text } from "@microsoft/sp-core-library";
import { CalendarServiceProviderList } from "../../services/CalendarService/CalendarServiceProviderList";
import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import CalendarFeedSummary from "../../../eventCustom/components/CalendarFeedSummary";
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import * as ReactDOM from "react-dom";
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { getId } from 'office-ui-fabric-react/lib/Utilities';
import { ContextualMenu } from 'office-ui-fabric-react/lib/ContextualMenu';
import { hiddenContentStyle, mergeStyles } from 'office-ui-fabric-react/lib/Styling';
/**
 * Shows an event in a document card
 */
export class EventCard extends React.Component<IEventCardProps, IEventCardState> {
  _providerList: any;
  constructor(props: IEventCardProps) {
    super(props);
    this.state = {
      hideDialog: true,
    };
  }

  public render(): React.ReactElement<IEventCardProps> {
      const { isNarrow } = this.props;

      // if (isNarrow) {
      //     return this._renderNarrowCell();
      // } else {
          return this._renderNormalCell();
      // }
  }

  private _renderNormalCell(): JSX.Element {
      const {
          ID,
          start,
          end,
          allDay,
          title,
          url,
          category,
          location,
          inscricao,
          sorteio,
          Vagas,
          configuracaoTipo } = this.props.event;
      const { context } = this.props;

      console.log(this.props.event);

      let auxSubscribe = false;

      const eventDate: moment.Moment = moment(start);
      const dateString: string = allDay ? eventDate.format(strings.AllDayDateFormat) : eventDate.format(strings.LocalizedTimeFormat);
      const { isEditMode } = this.props;
      this.props.subscribers.map((data) => ( data.eventoId === ID ? auxSubscribe = true : '') ); //verifica se usuario ja esta inscrito
      return (
          <div>
              <div>
                <Dialog
                  hidden={this.state.hideDialog}
                  onDismiss={this._closeDialog}
                  dialogContentProps={{
                    type: DialogType.normal,
                    title: 'Inscrição',
                    closeButtonAriaLabel: 'Fechar',
                    subText: 'Evento Inscrito com sucesso'
                  }}
                  modalProps={{
                    titleAriaId: 'idLabel',
                    subtitleAriaId: 'idsubtitle',
                    isBlocking: false,
                    styles: { main: { maxWidth: 450 } },
                  }}
                >
                  <DialogFooter>
                    <DefaultButton onClick={this._closeDialog} text="Fechar" />
                  </DialogFooter>
                </Dialog>
              </div>
              <div
                  className={css(styles.cardWrapper)}
                  data-is-focusable={true}
                  data-is-focus-item={true}
                  role="listitem"
                  aria-label={Text.format(strings.EventCardWrapperArialLabel, title, `${dateString}`)}
                  tabIndex={0}
              >
                  <DocumentCard
                      className={css(styles.root, !isEditMode && styles.rootIsActionable, styles.normalCard)}
                      type={DocumentCardType.normal}
                      // onClickHref={isEditMode ? null : url}
                  >
                      <div className={styles.cardImgEvento}>
                        {(configuracaoTipo && configuracaoTipo.linkImagem) ?
                        <img
                        src={configuracaoTipo.linkImagem}
                        alt={'I'}
                        style={{backgroundColor: ((configuracaoTipo && configuracaoTipo.backgroundColor)? configuracaoTipo.backgroundColor: ''), width: "100%", fontSize: "250%", borderRadius: "90px", color: "white", minHeight: "100%"}}/>
                        :
                        <Icon style={{backgroundColor: ((configuracaoTipo && configuracaoTipo.backgroundColor)? configuracaoTipo.backgroundColor: ''), padding: "23%", fontSize: "250%", borderRadius: "90px", color: "white"}} iconName={(configuracaoTipo && configuracaoTipo.nomeIconFabric)? configuracaoTipo.nomeIconFabric: 'FrontCamera'} title="Event" ariaLabel="Event" />
                        }
                      </div>
                      <FocusZone>
                          <div className={styles.dateBoxContainer} style={{ height: 160 }} data-automation-id="normal-card-preview">
                              <DateBox
                                  className={styles.dateBox}
                                  startDate={start}
                                  endDate={end}
                                  size={DateBoxSize.Medium}
                              />
                          </div>
                          <div className={styles.detailsContainer}>
                              <div className={styles.category}>{category}</div>
                              <div className={styles.title} data-automation-id="event-card-title">{title}</div>
                              <div className={styles.datetime}>{dateString}</div>
                              <div className={styles.location}>{location}</div>
                              { (( !inscricao || inscricao === 'não') || auxSubscribe) ? '' :
                                (( sorteio && sorteio === 'não') && ( Vagas && Vagas === 0 )) ? 
                                <div className={'buttonSubscribe' + ID}>
                                  <Button id='buttonInsertSubscribe'
                                    className={styles.addToMyCalendar}
                                    ariaLabel={strings.AddToCalendarAriaLabel}
                                    // onClick={() => { this.subscribeEvent(ID); }}
                                  >
                                    {strings.AddToCalendarButtonLabelSubscribe}
                                  </Button>
                                </div>
                                :
                                <div className={'buttonSubscribe' + ID}>
                                  <Button id='buttonInsertSubscribe'
                                    className={styles.addToMyCalendar}
                                    //iconProps={{ iconName: "AddEvent" }}
                                    ariaLabel={strings.AddToCalendarAriaLabel}
                                    // onClick={this.subscribeEvent()}
                                    onClick={() => { this.subscribeEvent(ID); }}
                                  >
                                    {strings.AddToCalendarButtonLabel}
                                  </Button>
                                </div> 
                              }
                          </div>
                      </FocusZone>
                  </DocumentCard>
              </div>
          </div>
      );
  }

  private _showDialog = (): void => {
    this.setState({ hideDialog: false });
  };

  private _closeDialog = (): void => {
    this.setState({ hideDialog: true });
  };

  private _renderNarrowCell(): JSX.Element {
      const { start,
          end,
          allDay,
          title,
          url,
          // category,
          // location
        } = this.props.event;

      const eventDate: moment.Moment = moment.utc(start);
      const dateString: string = allDay ? eventDate.format(strings.AllDayDateFormat) : eventDate.format(strings.LocalizedTimeFormat);
      return (
          <div>
              <div
                  className={css(styles.cardWrapper, styles.compactCard, styles.root, styles.rootIsCompact)}
                  data-is-focusable={true}
                  data-is-focus-item={true}
                  role="listitem"
                  aria-label={Text.format(strings.EventCardWrapperArialLabel, title, dateString)}
              >
                  <DocumentCard
                      className={css(styles.root, styles.rootIsActionable, styles.rootIsCompact)}
                      type={DocumentCardType.compact}
                      // onClickHref={url}
                  >
                      <div data-automation-id="normal-card-preview">
                          <DateBox
                              className={styles.dateBox}
                              startDate={start}
                              endDate={end}
                              size={DateBoxSize.Small}
                          />
                      </div>
                      <div>
                          <div className={styles.title} data-automation-id="event-card-title">{title}</div>
                          <div className={styles.datetime}>{dateString}</div>
                      </div>
                  </DocumentCard>
              </div>
          </div>
      );
  }

  private subscribeEvent = (idEvent): void => {
    {
      const body: string = JSON.stringify({
        '__metadata': {
          'type': "SP.Data.TesteListItem" // select=ListItemEntityTypeFullName
          // 'type': "SP.RenderListDataParameters" // select=ListItemEntityTypeFullName
        },
        // 'evento': "1;Events",
        'eventoId': idEvent,
        'Title': `Pessoa Inscrita`,
        'PessoaId': this.props.context.pageContext.legacyPageContext.userId
        // 'Titulo': `Item ${new Date()}`
      });

      try {
        this.props.context.spHttpClient.post(`${this.props.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items`,
        SPHttpClient.configurations.v1,
        {
          headers: {
            'Accept': 'application/json;odata=nometadata',
            'Content-type': 'application/json;odata=verbose',
            'odata-version': ''
          },
          body: body
        }).then((response: SPHttpClientResponse) => {
          return response.json();
        })
        .then(async (item: any): Promise<void> => {
          // this.setState({
          //   status: `Item ID: ${item.Id}, Title: ${item.Title}`,
          //   items: []
          // });

          const aux = (
            <div></div>
          );

          const classNameElementButton = 'buttonSubscribe'+idEvent;
          const documentoGetElement = document.getElementsByClassName(classNameElementButton);

          for (let key = 0; key < documentoGetElement.length; key++) {
            if (documentoGetElement[key]) {
              await ReactDOM.render( aux, documentoGetElement[key]);
            }
          }

          this._showDialog();
          console.log("item", item);
        }, (error: any): void => {
          console.log("erro", error);
        });;

      }
      catch (error) {
        console.log("Exception caught by catch in SharePoint provider", error);
        throw error;
      }
    }
  }

  private _onAddToMyCalendar = (): void => {
      const { event } = this.props;

      // create a calendar to hold the event
      const cal: ICS.VCALENDAR = new ICS.VCALENDAR();
      cal.addProp("VERSION", 2.0);
      cal.addProp("PRODID", "//SPFX//NONSGML v1.0//EN");

      // create an event
      const icsEvent: ICS.VEVENT = new ICS.VEVENT();

      // generate a unique id
      icsEvent.addProp("UID", Guid.newGuid().toString());

      // if the event is all day, just pass the date component
      if (event.allDay) {
          icsEvent.addProp("DTSTAMP", event.start, { VALUE: "DATE" });
          icsEvent.addProp("DTSTART", event.start, { VALUE: "DATE" });
      } else {
          icsEvent.addProp("DTSTAMP", event.start, { VALUE: "DATE-TIME" });
          icsEvent.addProp("DTSTART", event.start, { VALUE: "DATE-TIME" });
          icsEvent.addProp("DTEND", event.start, { VALUE: "DATE-TIME" });
      }

      // add a title
      icsEvent.addProp("SUMMARY", event.title);

      // add a url if there is one
      if (event.url !== undefined) {
          icsEvent.addProp("URL", event.url);
      }

      // add a description if there is one
      if (event.description !== undefined) {
          icsEvent.addProp("DESCRIPTION", event.description);
      }

      // add a location if there is one
      if (event.location !== undefined) {
          icsEvent.addProp("LOCATION", event.location);
      }

      // add the event to the calendar
      cal.addComponent(icsEvent);

      // export the calendar
      // my spidey senses are telling me that there are sitaations where this isn't going to work, but none of my tests could prove it.
      // i suspect we're not encoding events properly
      window.open("data:text/calendar;charset=utf8," + encodeURIComponent(cal.toString()));
  }
}
