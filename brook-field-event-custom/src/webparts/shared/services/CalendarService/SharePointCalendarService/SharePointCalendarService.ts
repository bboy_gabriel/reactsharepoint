import { ISubscribe } from './../ISubscribe';
import { HttpClientResponse, SPHttpClient, HttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import { ICalendarService } from "..";
import { BaseCalendarService } from "../BaseCalendarService";
import { ICalendarEvent } from "../ICalendarEvent";
import { Web } from "@pnp/sp";
import { combine } from "@pnp/common";

export class SharePointCalendarService extends BaseCalendarService
  implements ICalendarService {
  constructor() {
    super();
    this.Name = "SharePoint";
  }

  public getEvents = async (): Promise<ICalendarEvent[]> => {
    const parameterizedFeedUrl: string = this.replaceTokens(
      this.FeedUrl,
      this.EventRange
    );

    // Get the URL
    let webUrl = parameterizedFeedUrl.toLowerCase();

    // Break the URL into parts
    let urlParts = webUrl.split("/");

    // Get the web root
    let webRoot = urlParts[0] + "/" + urlParts[1] + "/" + urlParts[2];

    // Get the list URL
    let listUrl = webUrl.substring(webRoot.length);

    // Find the "lists" portion of the URL to get the site URL
    let webLocation = listUrl.substr(0, listUrl.indexOf("lists/"));
    let siteUrl = webRoot + webLocation;

    // Open the web associated to the site
    let web = new Web(siteUrl);

    // Get the web
    await web.get();
    // Build a filter so that we don't retrieve every single thing unless necesssary
    let dateFilter: string = "EventDate ge datetime'" + this.EventRange.Start.toISOString() + "' and EndDate lt datetime'" + this.EventRange.End.toISOString() + "'";
    try {
      const items = await web.getList(listUrl)
        .items.
        // getAll();
        select("Id,Title,Description,EventDate,EndDate,fAllDayEvent,Category,Location,Vagas,AuthorId,Inscricao,Sorteio,configuracaoTipo/backgroundColor,configuracaoTipo/nomeIconFabric,configuracaoTipo/linkImagem &$expand=configuracaoTipo")
        .orderBy('EventDate', true)
        .filter(dateFilter)
        .get();

        console.log(items);
        
      // Once we get the list, convert to calendar events
      let events: ICalendarEvent[] = items.map((item: any) => {
        let eventUrl: string = combine(webUrl, "DispForm.aspx?ID=" + item.Id);
        const eventItem: ICalendarEvent = {
          ID: item.ID,
          title: item.Title,
          start: item.EventDate,
          end: item.EndDate,
          url: eventUrl,
          allDay: item.fAllDayEvent,
          category: item.Category,
          description: item.Description,
          location: item.Location,
          Vagas: item.Vagas,
          AuthorId: item.AuthorId,
          configuracaoTipo: item.configuracaoTipo,
          inscricao: item.Inscricao,
          sorteio: item.Sorteio,
        };
        return eventItem;
      });
      // Return the calendar items
      return events;
    }
    catch (error) {
      console.log("Exception caught by catch in SharePoint provider", error);
      throw error;
    }
  }

  public getSubscribers = async (): Promise<ISubscribe[]> => {
    const parameterizedFeedUrl: string = this.replaceTokens(
      this.FeedUrl,
      this.EventRange
    );

    // Build a filter so that we don't retrieve every single thing unless necesssary
    let dateFilter: string = "EventDate ge datetime'" + this.EventRange.Start.toISOString() + "' and EndDate lt datetime'" + this.EventRange.End.toISOString() + "'";
    try { //?$expand=FieldValuesAsText,FieldValuesAsHtml"
      let events: ISubscribe[];//$filter=AuthorId eq 20 // &$select=Author/FirstName,Author/Id,Author/Name,Author/JobTitle &$expand=Author
      await this.Context.httpClient.get(`${this.Context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items?$filter=AuthorId eq ${this.Context.pageContext.legacyPageContext.userId}`,
      SPHttpClient.configurations.v1,
      {
        headers: {
          'Accept': 'application/json;odata=nometadata',
          'odata-version': ''
        }
      }).then((response: SPHttpClientResponse) => {
        return response.json();
      })
      .then(async (items): Promise<any> => {
        // this.setState({
        //   status: `Item ID: ${item.Id}, Title: ${item.Title}`,
        //   items: []
        // });
        console.log("item", items);
        events = await items.value;
        // value.forEach((element): ISubscribe => {
        //   const auxSub: ISubscribe = {
        //     AuthorId : element.AuthorId,
        //     ID : element.ID,
        //     Title : element.Title,
        //     eventoId : element.eventoId,
        //   };
        //   return auxSub;
        // });
        // itemsSubs = items;
        // events = items;
      }, (error: any): void => {
        console.log("erro", error);
      });

      return events;
    }
    catch (error) {
      console.log("Exception caught by catch in SharePoint provider", error);
      throw error;
    }
  }

  public setSubscribers = async (idEvent: string): Promise<any[]> => {
    const body: string = JSON.stringify({
      '__metadata': {
        'type': "SP.Data.TesteListItem" // select=ListItemEntityTypeFullName
        // 'type': "SP.RenderListDataParameters" // select=ListItemEntityTypeFullName
      },
      // 'evento': "1;Events",
      'eventoId': idEvent,
      'Title': `Pessoa Inscrita`,
      'PessoaId': this.Context.pageContext.legacyPageContext.userId
      // 'Titulo': `Item ${new Date()}`
    });

    try {
      const itemsTeste = this.Context.spHttpClient.post(`${this.Context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items`,
      SPHttpClient.configurations.v1,
      {
        headers: {
          'Accept': 'application/json;odata=nometadata',
          'Content-type': 'application/json;odata=verbose',
          'odata-version': ''
        },
        body: body
      }).then((response: SPHttpClientResponse) => {
        return response.json();
      })
      .then((item: any): void => {
        // this.setState({
        //   status: `Item ID: ${item.Id}, Title: ${item.Title}`,
        //   items: []
        // });
        console.log("item", item);
      }, (error: any): void => {
        console.log("erro", error);
      });;

      // Once we get the list, convert to calendar events
      let events: ICalendarEvent[]
      return events;
    }
    catch (error) {
      console.log("Exception caught by catch in SharePoint provider", error);
      throw error;
    }
  }
}
