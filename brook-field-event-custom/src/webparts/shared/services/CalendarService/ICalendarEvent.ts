export interface ICalendarEvent {
    ID: any;
    title: string;
    start: Date;
    end: Date;
    url: string|undefined;
    allDay: boolean;
    category: string|undefined;
    description: string|undefined;
    location: string|undefined;
    Vagas?: number|undefined;
    AuthorId?: number|undefined;
    configuracaoTipo?: any |undefined;
    inscricao?: string|'não';
    sorteio?: string|'não';
}
