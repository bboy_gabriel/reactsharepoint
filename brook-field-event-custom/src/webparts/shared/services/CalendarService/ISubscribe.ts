import { IWebPartContext } from "@microsoft/sp-webpart-base";
import { CalendarEventRange, ICalendarEvent } from ".";

export interface ISubscribe {
    AuthorId: any;
    ID: any;
    Title: any;
    eventoId: any;
    Author?: any
    sorteio: string| undefined;
}
