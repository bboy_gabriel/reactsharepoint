import { DisplayMode } from "@microsoft/sp-core-library";
import { SPComponentLoader } from "@microsoft/sp-loader";
import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { WebPartTitle } from "@pnp/spfx-controls-react/lib/WebPartTitle";
import * as strings from 'EventCustomWebPartStrings';
import * as moment from "moment";
import { FocusZone, FocusZoneDirection, List, Spinner, css, Label } from "office-ui-fabric-react";
import * as React from "react";
import { EventCard } from "../../shared/components/EventCard";
import { Paging } from "../../shared/components/Paging";
import { IconButton, PrimaryButton } from 'office-ui-fabric-react/lib/Button';
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import { CalendarServiceProviderType, ICalendarEvent, ICalendarService } from "../../shared/services/CalendarService";
import styles from "./CalendarFeedSummary.module.scss";
import { ICalendarFeedSummaryProps, ICalendarFeedSummaryState, IFeedCache } from "./CalendarFeedSummary.types";
import { FilmstripLayout } from "../../shared/components/filmstripLayout/index";
import { IPersonaSharedProps, Persona, PersonaInitialsColor, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import { Stack } from 'office-ui-fabric-react/lib/Stack';

import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Panel } from 'office-ui-fabric-react/lib/Panel';
// import { useConstCallback } from '@uifabric/react-hooks';
import { SPHttpClient, SPHttpClientResponse } from "@microsoft/sp-http";
import { ISubscribe } from "../../shared/services/CalendarService/ISubscribe";
import * as ReactDOM from "react-dom";

// the key used when caching events
const CacheKey: string = "calendarFeedSummary";

/**
 * Displays a feed summary from a given calendar feed provider. Renders a different view for mobile/narrow web parts.
 */
export default class CalendarFeedSummary extends React.Component<ICalendarFeedSummaryProps, ICalendarFeedSummaryState> {
  constructor(props: ICalendarFeedSummaryProps) {
    super(props);
    this.state = {
      isLoading: false,
      events: [],
      subscribers: [],
      subscribersList: [],
      error: undefined,
      openPanel: false,
      currentPage: 1
    };
  }

  /**
   * When components are mounted, get the events
   */
  public componentDidMount(): void {
    if (this.props.isConfigured) {
      this._loadEvents(true);
    }
  }

  /**
   * When someone changes the property pane, it triggers this event. Use it to determine if we need to refresh the events or not
   * @param prevProps The previous props before changes are applied
   * @param prevState The previous state before changes are applied
   */
  public componentDidUpdate(prevProps: ICalendarFeedSummaryProps, prevState: ICalendarFeedSummaryState): void {
    // only reload if the provider info has changed
    const prevProvider: ICalendarService = prevProps.provider;
    const currProvider: ICalendarService = this.props.provider;

    // if there isn't a current provider, do nothing
    if (currProvider === undefined) {
      return;
    }

    // if we didn't have a provider and now we do, we definitely need to update
    if (prevProvider === undefined) {
      if (currProvider !== undefined) {
        this._loadEvents(false);
      }

      // there's nothing to do because there isn't a provider
      return;
    }

    const settingsHaveChanged: boolean = prevProvider.CacheDuration !== currProvider.CacheDuration ||
      prevProvider.Name !== currProvider.Name ||
      prevProvider.FeedUrl !== currProvider.FeedUrl ||
      prevProvider.Name !== currProvider.Name ||
      prevProvider.EventRange.DateRange !== currProvider.EventRange.DateRange ||
      prevProvider.UseCORS !== currProvider.UseCORS ||
      prevProvider.MaxTotal !== currProvider.MaxTotal ||
      prevProvider.ConvertFromUTC !== currProvider.ConvertFromUTC;

    if (settingsHaveChanged) {
      // only load from cache if the providers haven't changed, otherwise reload.
      this._loadEvents(false);
    }
  }

  /**
   * Renders the view. There can be three different outcomes:
   * 1. Web part isn't configured and we show the placeholders
   * 2. Web part is configured and we're loading events, or
   * 3. Web part is configured and events are loaded
   */
  public render(): React.ReactElement<ICalendarFeedSummaryProps> {
    const {
      isConfigured,
    } = this.props;

    // if we're not configured, show the placeholder
    if (!isConfigured) {
      return <Placeholder
        iconName="Calendar"
        iconText={strings.PlaceholderTitle}
        description={strings.PlaceholderDescription}
        buttonLabel={strings.ConfigureButton}
        onConfigure={this._onConfigure} />;
    }

    // put everything together in a nice little calendar view
    return (
      <div><Spinner label={strings.Loading}  className={styles.spinner + ' ' + ((this.state.isLoading) ? styles.displayBlock : styles.displayNone )} />
        <div className={css(styles.calendarFeedSummary, styles.webPartChrome)}>
          <div>
            <Panel
              headerText="Lista dos Inscritos"
              isOpen={this.state.openPanel}
              // onDismiss={dismissPanel}
              closeButtonAriaLabel="Close"
              onRenderFooterContent={ () => { return(
                <div>
                  <PrimaryButton onClick={() => { this.saveLucklyPeople(); }}>
                    Salvar Sorteio
                  </PrimaryButton>
                  <DefaultButton className={css(styles.buttonPanelInscricao)} onClick={() => { this.setState({ openPanel: false }) }}>Fechar</DefaultButton>
                  <DefaultButton className={css(styles.buttonPanelInscricao)} onClick={() => { this.goTo('editar', this.state.eventSelected.ID); }}>Editar</DefaultButton>
                </div>)
              } }
              onOpened={ () => { this._renderSubscribersList(); } }
            >
              <div id='paneListSubscribe'></div>
            </Panel>
          </div>
          <div className={css(styles.webPartHeader, styles.headerSmMargin)}>
            <Icon className={css(styles.iconCalendarHeader)} iconName='Calendar' title="Event" ariaLabel="Event" />
            <WebPartTitle displayMode={this.props.displayMode}
              title={this.props.title}
              updateProperty={this.props.updateProperty}
            /> 
          </div>
          <div className={css(styles.webPartHeader, styles.headerSmMargin)}>
            <div className={css(styles.lineTitleHeader)}></div>
          </div>
          <div className={css(styles.headerSmMargin)}>
            <div className={css(styles.floatLeft)} onClick={() => { this.goTo('add'); }}>
              <IconButton className={css(styles.colorGreen)} iconProps={{ iconName: 'Add' }} title="Adicionar Evento" ariaLabel="Adicionar Evento" /><span className={css(styles.colorWhite)}>Adicionar Evento </span>
            </div>
            <div className={css(styles.floatRight)} onClick={() => { this.goTo('listarAll'); }}>
              <span className={css(styles.colorWhite)}>Ver todos</span>
            </div>
          </div>
          <div className={styles.content}>
            {this._renderContent()}
          </div>
        </div>
      </div>
    );
  }

  private goTo(url,id = null) {
    const link = document.createElement('a');
    link.target = '';
    if(url === 'add'){
      link.href = this.props.context.pageContext.site.absoluteUrl + '/Lists/Events/newForm.aspx?Source=' + this.props.context.pageContext.site.absoluteUrl;
    } else if(url === 'editar' && id){
      link.href = this.props.context.pageContext.site.absoluteUrl + '/Lists/Events/DispForm.aspx?ID='+ 2 +'&Source=' + this.props.context.pageContext.site.absoluteUrl;
    } else {
      link.href = this.props.context.pageContext.site.absoluteUrl + '/Lists/Events/calendar.aspx?Source=' + this.props.context.pageContext.site.absoluteUrl;
    }
    document.body.appendChild(link);
    link.click();
  }

  /**
   * Render your web part content
   */
  private _renderContent(): JSX.Element {
    const {
      isNarrow,
      displayMode
    } = this.props;
    const {
      events,
      subscribers,
      isLoading,
      error
    } = this.state;

    const isEditMode: boolean = displayMode === DisplayMode.Edit;
    const hasErrors: boolean = error !== undefined;
    const hasEvents: boolean = events.length > 0;

    // if (isLoading) {
      // we're currently loading
      // return (<div className={styles.spinner}><Spinner label={strings.Loading} /></div>);
    // }

    if (hasErrors) {
      // we're done loading but got some errors
      if (!isEditMode) {
        // otherwise, just show a friendly message
        return (<div className={styles.errorMessage}>{strings.ErrorMessage}</div>);
      } else {
        // render a more advanced diagnostic of what went wrong
        return this._renderError();
      }
    }

    if (!hasEvents) {
      // we're done loading, no errors, but have no events
      return (<div className={styles.emptyMessage}>{strings.NoEventsMessage}</div>);
    }

    // we're loaded, no errors, and got some events
    // if (isNarrow) {
    //   return this._renderNarrowList();
    // } else {
      return this._renderNormalList();
    // }
  }

  /**
   * Tries to make sense of the returned error messages and provides
   * (hopefully) helpful guidance on how to fix the issue.
   * It isn't the best piece of coding I've seen. I'm open to suggested improvements
   */
  private _renderError(): JSX.Element {
    const { error } = this.state;
    const { provider } = this.props;
    let errorMsg: string = strings.ErrorMessage;
    switch (error) {
      case "Not Found":
        errorMsg = strings.ErrorNotFound;
        break;
      case "Failed to fetch":
        if (!provider.UseCORS) {
          // maybe it is because of mixed content?
          if (provider.FeedUrl.toLowerCase().substr(0, 7) === "http://") {
            errorMsg = strings.ErrorMixedContent;
          } else {
            errorMsg = strings.ErrorFailedToFetchNoProxy;
          }
        } else {
          errorMsg = strings.ErrorFailedToFetch;
        }
        break;
      default:
        // specific provider messages
        if (provider.Name === CalendarServiceProviderType.RSS) {
          switch (error) {
            case "No result":
              errorMsg = strings.ErrorRssNoResult;
              break;
            case "No root":
              errorMsg = strings.ErrorRssNoRoot;
              break;
            case "No channel":
              errorMsg = strings.ErrorRssNoChannel;
              break;
          }
        } else if (provider.Name === CalendarServiceProviderType.iCal &&
          error.indexOf("Unable to get property 'property' of undefined or null reference") !== -1) {
          errorMsg = strings.ErrorInvalidiCalFeed;
        } else if (provider.Name === CalendarServiceProviderType.WordPress && error.indexOf("Failed to read") !== -1) {
          errorMsg = strings.ErrorInvalidWordPressFeed;
        }
    }

    return (<div className={styles.errorMessage} >
      <div className={styles.moreDetails}>
        {errorMsg}
      </div>
    </div>);
  }

  /**
   * Renders a narrow view of the calendar feed when the webpart is less than 480 pixels
   */
  private _renderNarrowList(): JSX.Element {
    const {
      events,
      subscribers,
      currentPage
    } = this.state;

    const { maxEvents } = this.props;

    // if we're in edit mode, let's not make the events clickable
    const isEditMode: boolean = this.props.displayMode === DisplayMode.Edit;

    let pagedEvents: ICalendarEvent[] = events;
    let usePaging: boolean = false;

    if (maxEvents > 0 && events.length > maxEvents) {
      // calculate the page size
      const pageStartAt: number = maxEvents * (currentPage - 1);
      const pageEndAt: number = (maxEvents * currentPage);

      pagedEvents = events.slice(pageStartAt, pageEndAt);
      usePaging = true;
    }

    return (<FocusZone
      direction={FocusZoneDirection.vertical}
      isCircularNavigation={false}
      data-automation-id={"narrow-list"}
      aria-label={isEditMode ? strings.FocusZoneAriaLabelEditMode : strings.FocusZoneAriaLabelReadMode}
    >
      <List
        items={pagedEvents}
        onRenderCell={(item, index) => (
          <div onClick={(e) => { this.listSubscribePeople(e, item) }}>
            <EventCard
              isEditMode={isEditMode}
              event={item}
              subscribers={subscribers}
              context={this.props.context}
              isNarrow={true} />
          </div>
        )} />
      {usePaging &&
        <Paging
          showPageNum={false}
          currentPage={currentPage}
          itemsCountPerPage={maxEvents}
          totalItems={events.length}
          onPageUpdate={this._onPageUpdate} />
      }
    </FocusZone>
    );
  }

  private _onPageUpdate = (pageNumber: number): void => {
    this.setState({
      currentPage: pageNumber
    });
  }

  private async listSubscribePeople(e , Event) {
    console.log(e.target.firstChild);
    console.log(e.target);
    
    if ( this.props.context.pageContext.legacyPageContext.userId === Event.AuthorId ) { //verifica se é o criador do evento

      if( (e.target.firstChild !== null && e.target.firstChild) && 
          ((e.target.firstChild.innerText === 'Subscribe' || e.target.firstChild.innerText === 'Inscrever') ||
          (e.target.firstChild.data === 'Subscribe' || e.target.firstChild.data === 'Inscrever') ) ) {
        return false;
      }
    } else {return false;}

    this.setState({
      isLoading: true,
    });

    try {
      await this.props.context.httpClient.get(`${this.props.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items?$filter=eventoId eq ${Event.ID} &$select=AuthorId,ID,Title,sorteio,eventoId,Author/FirstName,Author/Id,Author/Name,Author/JobTitle &$expand=Author &$orderBy= sorteio`,
      SPHttpClient.configurations.v1,
      {
        headers: {
          'Accept': 'application/json;odata=nometadata',
          'odata-version': ''
        }
      }).then((response: SPHttpClientResponse) => {
        this.setState({
          etag : response.headers.get('ETag')
        })
        return response.json();
      })
      .then(async (items): Promise<any> => {
        this.setState({
          eventSelected: Event,
          subscribersList: items.value,
          openPanel: true,
          isLoading: false,
        });
      }, (error: any): void => {
        console.log("erro", error);
      });
    }
    catch (error) {
      console.log("Exception caught by catch in SharePoint provider", error);
      throw error;
    }

  }

  private async saveLucklyPeople() {
    try {
      this.setState({
        openPanel: false,
        // isLoading: true
      });
      this.state.subscribersList.forEach(element => {
        const body: string = JSON.stringify({
          '__metadata': {
            'type': "SP.Data.TesteListItem" // select=ListItemEntityTypeFullName
            // 'type': "SP.RenderListDataParameters" // select=ListItemEntityTypeFullName
          },
          'Title': (element.sorteio === 'false') ? "Pessoa não inscrita": "Pessoa inscrita",
          'sorteio': (element.sorteio === 'false') ? element.sorteio+ '': '',
        });

        // this.props.context.httpClient.get(`${this.props.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items(${element.ID})?$select=ID,Title,sorteio`,
        this.props.context.spHttpClient.post(`${this.props.context.pageContext.web.absoluteUrl}/_api/web/lists/getbytitle('Teste')/items(${element.ID})?$select=ID,Title,sorteio`,
        SPHttpClient.configurations.v1,
        {
          // method: 'POST',
          headers: {
            'Accept': 'application/json;odata=nometadata',
            'Content-type': 'application/json;odata=verbose',
            'odata-version': '',
            'IF-MATCH': '*',
            'X-HTTP-Method': 'MERGE'
          },
          body: body
        }).then((response: SPHttpClientResponse) => {
          return response.json();
        })
        .then(async (items): Promise<any> => {

        }, (error: any): void => {
          console.log("erro", error);
          this.setState({
            openPanel: false,
          });
        }); // httpClient

      }); // foreach
    }
    catch (error) {
      console.log("Exception caught by catch in SharePoint provider", error);
      this.setState({
        openPanel: false,
      });
      throw error;
    }

  }

  /**
   * Render a normal view for devices that are wider than 480
   */
  private _renderNormalList(): JSX.Element {
    const {
      events, subscribers } = this.state;
    const isEditMode: boolean = this.props.displayMode === DisplayMode.Edit;

    return (<div>
      <div>
        <div role="application">
          <FilmstripLayout
            ariaLabel={strings.FilmStripAriaLabel}
          >
            {events.map((event: ICalendarEvent, index: number) => {
              return (
                <div onClick={(e) => { this.listSubscribePeople(e, event) }}>
                <EventCard
                  key={`eventCard${index}`}
                  isEditMode={isEditMode}
                  event={event}
                  subscribers={subscribers}
                  context={this.props.context}
                  isNarrow={false} />
                </div>
                );
            })}
          </FilmstripLayout>
        </div>
      </div>
    </div>);
  }

  private lucklySubscribeList(vagasQnt: number) {
    let inputArray: any[] = this.state.subscribersList;
    for (let i: number = inputArray.length - 1; i >= 0; i--){
        var randomIndex: number = Math.floor(Math.random() * (i + 1));
        var itemAtIndex: number = inputArray[randomIndex];

        inputArray[randomIndex] = inputArray[i];
        inputArray[i] = itemAtIndex;
    }

    if(vagasQnt && vagasQnt < inputArray.length){
      vagasQnt = inputArray.length - vagasQnt;
      for (let i: number = inputArray.length - 1; i >= 0; i--){
        if(vagasQnt > 0){
          inputArray[i].sorteio = 'false';
        } else {
          inputArray[i].sorteio = '';
        }
        vagasQnt--;
      }
    }

    this.setState({
      subscribersList: inputArray
    })
    this._renderSubscribersList();
    console.log(inputArray);
  }

  private async _renderSubscribersList() {

    const examplePersona: IPersonaSharedProps = {
      secondaryText: 'Designer',
      tertiaryText: 'In a meeting',
      optionalText: 'Available at 4:00pm'
    };

    const aux = await (
      <div>
        {this.state.eventSelected.Vagas &&
        <div>
          <div>
            <Label className={styles.floatRightPanelLabel}>Vagas: {this.state.eventSelected.Vagas}</Label>
          </div>
          <div>
            <PrimaryButton text="Sortear" onClick={() => { this.lucklySubscribeList(this.state.eventSelected.Vagas); }} />
          </div>
        </div>
        }
        <hr></hr>
        <Stack tokens={{ childrenGap: 10 }}>
          {this.state.subscribersList.map(function(list, index) { // BoxMultiplySolid
            return <div><Persona className={styles.floatLeftPanelLabel + ' ' + styles.width95} {...examplePersona} text={list.Author.FirstName + list.ID} size={PersonaSize.size24} /><IconButton className={styles.width5 + ' ' + ((list.sorteio === 'false') ? styles.colorRed: styles.colorGreen)} iconProps={{ iconName: 'BoxCheckmarkSolid' }} title={(list.sorteio === 'false' ? 'Não inscrito': 'inscrito')} ariaLabel={(list.sorteio === 'false' ? 'Não inscrito': 'inscrito')} /></div>
          })}
        </Stack>
      </div>
    );

    ReactDOM.render(aux, document.getElementById('paneListSubscribe'));
    this.setState({
      isLoading: false,
    });
  }

  /**
   * When users click on the Configure button, we display the property pane
   */
  private _onConfigure = () => {
    this.props.context.propertyPane.open();
  }

  /**
   * Load events from the cache or, if expired, load from the event provider
   */
  private async _loadEvents(useCacheIfPossible: boolean): Promise<void> {
    // before we do anything with the data provider, let's make sure that we don't have stuff stored in the cache

    // load from cache if: 1) we said to use cache, and b) if we have something in cache
    if (useCacheIfPossible && localStorage.getItem(CacheKey)) {
      let feedCache: IFeedCache = JSON.parse(localStorage.getItem(CacheKey));

      const { Name, FeedUrl } = this.props.provider;
      const cacheStillValid: boolean = moment().isBefore(feedCache.expiry);

      // make sure the cache hasn't expired or that the settings haven't changed
      if (cacheStillValid && feedCache.feedType === Name && feedCache.feedUrl === FeedUrl) {
        this.setState({
          isLoading: false,
          events: feedCache.events
        });
        return;
      }
    }

    // nothing in cache, load fresh
    const dataProvider: ICalendarService = this.props.provider;
    if (dataProvider) {

      try {
        let events = await dataProvider.getEvents();
        if (dataProvider.MaxTotal > 0) {
          events = events.slice(0, dataProvider.MaxTotal);
        }

        let subscribers = await dataProvider.getSubscribers();
        // don't cache in the case of errors
        this.setState({
          isLoading: false,
          error: undefined,
          events: events,
          subscribers: subscribers,
        });
        return;
      }
      catch (error) {
        console.log("Exception returned by getEvents", error.message);
        this.setState({
          isLoading: false,
          error: error.message,
          events: [],
          subscribers: []
        });
      }
    }
  }
}
