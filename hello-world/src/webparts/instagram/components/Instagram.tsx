import * as React from 'react';
import styles from '../InstagramWebPart.module.scss';
import { InstagramProps } from './InstagramProps';
import Grid from '@material-ui/core/Grid';
import ButtonBase from '@material-ui/core/ButtonBase';
import withInstagramFeed from 'origen-react-instagram-feed';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { TaxonomyPicker, IPickerTerms } from "@pnp/spfx-controls-react/lib/TaxonomyPicker";
import { PeoplePicker } from "@pnp/spfx-controls-react/lib/PeoplePicker";
// import ILeadsService from '../models/ILeadsService';
// import { Instagram } from "../Model/Instagram";
// import { default as pnp, ItemAddResult } from "sp-pnp-js";
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/components/Button';
import { Toggle } from 'office-ui-fabric-react/lib/Toggle';
import { Dropdown, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';

import { RxJsEventEmitter } from "../../libraries/rxJsEventEmitter/RxJsEventEmitter";
import { EventData } from "../../libraries/rxJsEventEmitter/EventData";
import getInstagramFeedInfo from './InstagramSevice';
import { any } from 'prop-types';

export default class Instagram extends React.Component<InstagramProps, InstagramProps> {

  private readonly _eventEmitter: RxJsEventEmitter = RxJsEventEmitter.getInstance();

  constructor(props) {
    super(props);
    this.state = {
      result: {account: any,
        accountInfo: any,
        accountFollowedBy: any,
        accountFollow: any,
        postsCount: any,
        profilePic: any,
        accountName: any,
        media: [],},
      account: any,
      accountInfo: any,
      accountFollowedBy: any,
      accountFollow: any,
      postsCount: any,
      profilePic: any,
      accountName: any,
      media: [],
    }
  }

  async componentDidMount() {
    await getInstagramFeedInfo("brooksfieldbr")
      .then(result => {
        // Will send this state as props to the component
        this.setState({ result })
        console.log(this.state);
        
      })
      .catch(() => {
        // updateStatus('failed');
      });
  }

  // componentDidMount() {
  //   this.setState({getInstagramFeedInfo("origenstudio")});
  // }

  public render(): React.ReactElement {

    return (
      <div>
        <Grid container spacing={1} >
        { this.state.result.media.map((data,index) => (
          <Grid item xs={12} sm={6} md={6} key={data[index].id || data[index].displayImage}>
            <Grid item xs={12} sm={12} md={12}>
              <ButtonBase className={styles.buttonBase}
                href={data[index].postLink }
              >
                <img
                  src={data[index].displayImage}
                  alt={data[index].accessibilityCaption || 'Instagram picture'}
                  className={styles.image}
                />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm={12} md={12}>
              <span>{data[index].caption }</span>
            </Grid>
          </Grid>
        // {status === 'loading' && <p>loading...</p>}
        // {status === 'failed' && <p>Check instagram here</p>}
        ))}
      </Grid>
      </div>
    );
  }
}



