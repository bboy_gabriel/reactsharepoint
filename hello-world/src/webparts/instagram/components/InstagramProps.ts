import { WebPartContext } from '@microsoft/sp-webpart-base';
import { SPHttpClient } from '@microsoft/sp-http';
import { string } from 'prop-types';

export interface InstagramProps {
  result?:any;
  account?: any;
  accountInfo: any,
  accountFollowedBy: any,
  accountFollow: any,
  postsCount: any,
  profilePic: any,
  accountName: any,
  media: Array<{
    id: string,
    displayImage: string,
    thumbnail: string,
    likes: number,
    caption: string,
    commentsNumber: number,
    accessibilityCaption: string,
    dimensions: { width: number, height: number },
    postLink: string,
  }>,
}
