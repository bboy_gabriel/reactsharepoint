import { InstagramProps } from './InstagramProps';
import Axios from 'axios';
import {get, slice} from 'lodash';
// import slice from 'lodash/slice';

/**
 * This function returns a promise that when resolves return data extracted from the public profile page of an instagram account.
 */
async function getInstagramFeedInfo(
  account: string,
  options: {
    numberOfMediaElements: number,
    discardVideos: boolean,
  } = {
    numberOfMediaElements: 12,
    discardVideos: false,
  },
): Promise<InstagramProps> {
  let media = [];
  let result = [];
  let accountInfo = {}; 

  try {
    const userInfoSource = await Axios.get(
      `https://www.instagram.com/${account}/`,
    );
    // userInfoSource.data contains the HTML from Axios
    const jsonObject = userInfoSource.data
      .match(
        /<script type="text\/javascript">window\._sharedData = (.*)<\/script>/,
      )[1]
      .slice(0, -1);


    accountInfo = JSON.parse(jsonObject).entry_data.ProfilePage[0].graphql.user;
    // .entry_data.ProfilePage[0].graphql.user;

    // Retrieve media info
    const mediaArray = slice(
      JSON.parse(jsonObject).entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges,
      0,
      options.numberOfMediaElements,
    );

    let result = [];

    media = mediaArray.map(function(value){
      // Process only if is an image
      result.push({
        id: get(value, 'node.id'),
        displayImage: get(value, 'node.display_url'),
        thumbnail: get(value, 'node.thumbnail_src'),
        likes: get(value, 'node.edge_liked_by.count'),
        caption: get(value, 'node.edge_media_to_caption.edges[0].node.text'),
        commentsNumber: get(value, 'node.edge_media_to_comment.count'),
        accessibilityCaption: get(value, 'node.accessibility_caption'),
        dimensions: get(value, 'node.dimensions'),
        postLink: `https://www.instagram.com/p/${get(value, 'shortcode')}/`,
      });
      return result;
    })

  } catch (e) {
    throw new Error(`Unable to retrieve info. Reason: ${e.toString()}`);
  }

  return {
    "accountInfo": accountInfo,
    "accountFollowedBy": get(accountInfo, 'edge_followed_by.count'),
    "accountFollow": get(accountInfo, 'edge_follow.count'),
    "postsCount": get(accountInfo, 'edge_owner_to_timeline_media.count'),
    "profilePic": get(accountInfo, 'profile_pic_url_hd'),
    "accountName": get(accountInfo, 'username'),
    "media":media,
  };
}

export default getInstagramFeedInfo;
