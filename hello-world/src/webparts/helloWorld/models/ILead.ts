export default interface ILead {
  id: string;
  nome?: string;
  firstName?: string;
  lastName?: string;
  company?: string;
  emailAddress?: string;
}