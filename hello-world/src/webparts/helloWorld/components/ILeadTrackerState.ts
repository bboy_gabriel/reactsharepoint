import ILead from '../models/ILead';

export interface ILeadTrackerState {
  eventsList: Array<{ index: number, data: number }>;
  targetList: string;
  loading: boolean;
  leads: ILead[];
}