import * as React from 'react';
import styles from './LeadTracker.module.scss';
import { ILeadTrackerProps } from './ILeadTrackerProps';
import { ILeadTrackerState } from './ILeadTrackerState';
import { escape } from '@microsoft/sp-lodash-subset';

import ILead from '../models/ILead';
import IList from '../models/IList';
import ILeadsService from '../models/ILeadsService';
import MockLeadsService from '../services/MockLeadsService';
import SharePointLeadsService from '../services/SharePointLeadsService';

import { RxJsEventEmitter } from "../../libraries/rxJsEventEmitter/RxJsEventEmitter";
import { EventData } from "../../libraries/rxJsEventEmitter/EventData";

import {
  DetailsList,
  IColumn,
  DetailsListLayoutMode
} from 'office-ui-fabric-react';

const leadColumns: IColumn[] = [
  { key: 'id', fieldName: 'id', name: 'ID', minWidth: 12, maxWidth: 24 },
  { key: 'firstName', fieldName: 'firstName', name: 'First Name', minWidth: 24, maxWidth: 64 },
  { key: 'lastName', fieldName: 'lastName', name: 'Last Name', minWidth: 24, maxWidth: 64 },
  { key: 'company', fieldName: 'company', name: 'Company', minWidth: 64, maxWidth: 120 },
  { key: 'emailAddress', fieldName: 'emailAddress', name: 'Email', minWidth: 100, maxWidth: 240 }
];

export default class HelloWorld extends React.Component<ILeadTrackerProps, ILeadTrackerState> {

  private leadsService: ILeadsService =
    new SharePointLeadsService(this.props.spHttpClient, this.props.siteUrl);

  public state: ILeadTrackerState = {
    eventsList: [],
    targetList: this.props.targetListDefault,
    loading: false,
    leads: []
  };

  private readonly _eventEmitter: RxJsEventEmitter = RxJsEventEmitter.getInstance();

  constructor(props: ILeadTrackerProps) {
    super(props);

    // this.state = { eventsList: [] };

    // subscribe for event by event name.
    this._eventEmitter.on("myCustomEvent:start", this.receivedEvent.bind(this));
  }

  public render(): React.ReactElement<ILeadTrackerProps> {
    return (
      <div className={styles.leadTracker}>

        {(this.state.targetList === "") ?
          <div className={styles.messageContainer} >Select a list from the web part property pane</div> :
          (this.state.loading) ?
            <div className={styles.loadingContainer} >Calling to the SharePoint REST API</div> :
            <DetailsList
              items={this.state.leads}
              columns={leadColumns}
              setKey='set'
              layoutMode={DetailsListLayoutMode.fixedColumns}
            />
        }

      </div>
    );
  }

  /**
   * Method that handles Received event from the RxJsEventEmitter.
   * Changes the state of the eventsList by adding the new event data to the array.
   * @param data the event object of the raised event.
   */
  protected receivedEvent(data: EventData): void {
    console.log('data',data);


    // update the events list with the newly received data from the event subscriber.
    this.state.eventsList.push(
      {
        index: this.state.eventsList.length,
        data: data.currentNumber
      }
    );

    // set new state.
    this.setState((previousState: ILeadTrackerState, props: ILeadTrackerProps): ILeadTrackerState => {
      previousState.eventsList = this.state.eventsList;
      return previousState;
    });

  }

  public componentDidMount() {
    this.setState({ loading: true });
    this.leadsService.getLeads(this.state.targetList).then((leads: ILead[]) => {
      this.setState({ leads: leads, loading: false });
    });
  }

  public componentDidUpdate(prevProps: ILeadTrackerProps, prevState: ILeadTrackerState, prevContext: any): void {
    if (prevState.targetList != this.state.targetList) {
      this.setState({ loading: true });
      this.leadsService.getLeads(this.state.targetList).then((leads: ILead[]) => {
        this.setState({ leads: leads, loading: false });
      });
    }
  }
}
