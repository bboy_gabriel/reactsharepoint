import ILead from "../models/ILead";
import IList from "../models/IList";
import ILeadsService from "../models/ILeadsService";

import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';

export default class SharePointLeadsService implements ILeadsService {

  constructor(private spHttpClient: SPHttpClient, private siteUrl: string) {
  }

  public getLeads(targetList: string): Promise<ILead[]> {

    // let restUrl = this.siteUrl +
    //   `/_api/web/lists/getByTitle('${targetList}')/items/` +
    //   "?$select=Id,FirstName,Title,Company,Email";
    let restUrl = "http://localhost:3000/api/fabrica";

    return this.spHttpClient.get(restUrl, SPHttpClient.configurations.v1)
      .then(response => response.json())
      .then(response => {
        // return response.map(lead => <ILead>({
        return response.map(lead => <ILead>({
          id: lead.id,
          firstName: lead.nome,
          lastName: 'silva',
          company: 'stefanini',
          emailAddress: 'grsilva3@stefanini.com'
        }));
      });
  }

  public getLeadsLists(): Promise<IList[]> {

    // let restUrl = this.siteUrl + "/_api/web/lists/" +
    //   "?$select=Id,Title&$filter=BaseTemplate+eq+105";
    let restUrl = "http://localhost:3000/api/fabrica";

    return this.spHttpClient.get(restUrl, SPHttpClient.configurations.v1)
      .then(response => response.json())
      .then(response => {
        return response.map(list => <IList>({
          id: list.id,
          title: list.nome
        }));
      });
  }

}