import ILead from "../models/ILead";
import IList from "../models/IList";
import ILeadsService from "../models/ILeadsService";
import axios from 'axios'


import {
  SPHttpClient,
  SPHttpClientResponse,
  ISPHttpClientOptions
} from '@microsoft/sp-http';

export default class SharePointLeadsService implements ILeadsService {

  constructor(private spHttpClient: SPHttpClient, private siteUrl: string) {
  }

  public getLeads(targetList: string): Promise<ILead[]> {

    // let restUrl = this.siteUrl +
    //   `/_api/web/lists/getByTitle('${targetList}')/items/` +
    //   "?$select=Id,FirstName,Title,Company,Email";
    let restUrl = "http://localhost:3000/api/fabrica";

    return this.spHttpClient.get(restUrl, SPHttpClient.configurations.v1)
      .then(response => response.json())
      .then(response => {
        // return response.map(lead => <ILead>({
        return response.map(lead => <ILead>({
          id: lead.id,
          firstName: lead.nome,
          lastName: 'silva',
          company: 'stefanini',
          emailAddress: 'grsilva3@stefanini.com'
        }));
      });
  }

  public getLeadsLists(): Promise<IList[]> {

    // let restUrl = this.siteUrl + "/_api/web/lists/" +
    //   "?$select=Id,Title&$filter=BaseTemplate+eq+105";
    let restUrl = "http://localhost:3000/api/fabrica";

    return this.spHttpClient.get(restUrl, SPHttpClient.configurations.v1)
      .then(response => response.json())
      .then(response => {
        return response.map(list => <IList>({
          id: list.id,
          title: list.nome
        }));
      });
  }

  public setLists(): Promise<any> {

    // let restUrl = this.siteUrl + "/_api/web/lists/" +
    //   "?$select=Id,Title&$filter=BaseTemplate+eq+105";
    let restUrl = "http://localhost:3000/api/pessoa_tecnologia";

    let data = {
      nome: 1,
      descricao: 'descricao',
    }

    // const opt:ISPHttpClientOptions={headers: { 'Content-Type': 'application/json' }, body: '{ "nome": "Developer Workbench", "BaseTemplate": 100 }' };
    // Request URL: https://stefaninilatam.sharepoint.com/sites/portaldev/_api/web/GetList(@a1)/AddValidateUpdateItemUsingPath()?@a1=%27%2Fsites%2Fportaldev%2FLists%2FSPXData%27
    // return this.spHttpClient.post(restUrl, SPHttpClient.configurations.v1, opt)
    //   .then(response => response.json())
    //   .then(response => {
    //     return response.map(list => ({
    //       id: list.id,
    //       title: list.nome
    //     }));
    //   });

    return axios.post(restUrl,data);
  }

}
