import { WebPartContext } from '@microsoft/sp-webpart-base';
import { SPHttpClient } from '@microsoft/sp-http';

export interface IReactSpFxPnPProps {
  description: string;
  context: WebPartContext;
  siteUrl: string;
  spHttpClient: SPHttpClient | undefined;
}
