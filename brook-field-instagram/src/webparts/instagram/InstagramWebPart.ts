import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneButtonType
} from '@microsoft/sp-webpart-base';

import Instagram from './components/Instagram';

// import styles from './InstagramWebPart.module.scss';
// import { escape } from '@microsoft/sp-lodash-subset';
import * as strings from 'InstagramWebPartStrings';

export interface IInstagramWebPartProps {
  description: string;
  accountNameInstagram: string;
}

export default class InstagramWebPart extends BaseClientSideWebPart<IInstagramWebPartProps> {

  public render(): void {
    console.log(this);
    
    const element: React.ReactElement<IInstagramWebPartProps> = React.createElement(
      Instagram,
      {
        context: this.context,
        description: this.properties.description,
        accountNameInstagram: this.properties.accountNameInstagram,
        siteUrl: this.context.pageContext.web.absoluteUrl,
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected get disableReactivePropertyChanges(): boolean {
    // require an apply button on the property pane
    return true;
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {

    const {
      description,
      accountNameInstagram,
    } = this.properties;

    return {
      pages: [
        {
          displayGroupsAsAccordion:true,
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel,
                  // value: description
                }),
                PropertyPaneTextField('accountNameInstagram', {
                  label: strings.AccontNameFieldLabel,
                  // value: accountNameInstagram
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
