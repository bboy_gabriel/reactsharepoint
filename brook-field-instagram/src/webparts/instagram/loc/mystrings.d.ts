declare interface IInstagramWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AccontNameFieldLabel: string;
}

declare module 'InstagramWebPartStrings' {
  const strings: IInstagramWebPartStrings;
  export = strings;
}
