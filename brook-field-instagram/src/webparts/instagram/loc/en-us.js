define([], function() {
  return {
    "PropertyPaneDescription": "Descrição",
    "BasicGroupName": "Nome do grupo",
    "DescriptionFieldLabel": "Campo descrição",
    "AccontNameFieldLabel": "Nome da conta",
    "AccontQntFieldLabel": "Quantidade de feed"
  }
});