import * as React from 'react';
import styles from './Instagram.module.scss';
import { IInstagramProps } from './IInstagramProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { InstagramProps } from './InstagramProps';
import Grid from '@material-ui/core/Grid';
import ButtonBase from '@material-ui/core/ButtonBase';
import withInstagramFeed from 'origen-react-instagram-feed';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { DefaultButton, PrimaryButton, Stack, IStackTokens, Button } from 'office-ui-fabric-react';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { TaxonomyPicker, IPickerTerms } from "@pnp/spfx-controls-react/lib/TaxonomyPicker";
import { PeoplePicker } from "@pnp/spfx-controls-react/lib/PeoplePicker";

import getInstagramFeedInfo from './InstagramSevice';
import { any } from 'prop-types';

export default class Instagram extends React.Component<InstagramProps, InstagramProps> {

  constructor(props) {
    super(props);
    this.state = {
      result: {account: any,
        accountInfo: any,
        accountFollowedBy: any,
        accountFollow: any,
        postsCount: any,
        profilePic: any,
        accountName: any,
        media: [],},
      account: any,
      accountInfo: any,
      accountFollowedBy: any,
      accountFollow: any,
      postsCount: any,
      profilePic: any,
      accountName: any,
      media: [],
    }
  }

  async componentDidMount() {
    await getInstagramFeedInfo(this.props.accountNameInstagram)
      .then(result => {
        console.log(result);

        // Will send this state as props to the component
        this.setState({ result })
        console.log(this.state);

      })
      .catch(() => {
        // updateStatus('failed');
      });
  }

  async componentDidUpdate() {
    await getInstagramFeedInfo(this.props.accountNameInstagram)
      .then(result => {
        console.log(result);

        // Will send this state as props to the component
        this.setState({ result })
        console.log(this.state);

      })
      .catch(() => {
        // updateStatus('failed');
      });
  }

  private goTo(url) {
    const link = document.createElement('a');
    link.target = '';
    link.href = url;
    document.body.appendChild(link);
    link.click();
  }

  public render(): React.ReactElement {

    return (
      <div>
        <Grid container spacing={1} className={styles.colorBackGroundBlue}>
          <Grid container spacing={1} className={styles.textInstagramHeader}>
            <Grid item xs={2} sm={1} md={1} className={styles.gridSvgInstagram}>
              <div className={styles.svgInstagram}></div>
            </Grid>
            <Grid item xs={10} sm={10} md={10} className={styles.gridTextHeaderInstagram}>
              <Grid item xs={12} sm={12} md={12} >
                <div className={styles.marginBottom10}>Instagram</div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} >
                <div className={styles.barraPerfilInstagram}></div>
              </Grid>
            </Grid>
          </Grid>
          <Grid container spacing={1} className={styles.gridPerfil}>
            { (!this.state.result.accountInfo.username) ? 'Usuario não encontrado' : 
            <Grid container spacing={1} className={styles.gridPerfil}>
              <Grid item xs={12} sm={3} md={3} >
                <ButtonBase className={styles.buttonBase}
                  href={this.state.account.external_url }>
                  <img
                    src={this.state.result.accountInfo.profile_pic_url}
                    alt={this.state.result.accountInfo.profile_pic_url || 'Instagram picture'}
                    className={styles.imagePerfil}/>
                  </ButtonBase>
              </Grid>
              <Grid item xs={12} sm={9} md={9} className={styles.discribePerfil}>
                <Grid item xs={12} sm={10} md={12}>
                  <span>
                    <div  className={styles.subscribeButton} onClick={() => { this.goTo('https://www.instagram.com/'+ this.state.result.accountInfo.username); }}>Seguir</div>
                    {this.state.result.accountInfo.full_name}
                  </span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span className={styles.paddingRight}>{this.state.result.postsCount +' publicações'} </span>
                  <span className={styles.paddingRight}>{this.state.result.accountFollowedBy +' seguidores'}</span>
                  <span className={styles.paddingRight}>{this.state.result.accountFollow +' seguindo'}</span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span>{this.state.result.accountInfo.biography} </span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span>Estágio: Link abaixo</span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <Link className={styles.textColorBlue} href={this.state.result.accountInfo.external_url}> {this.state.result.accountInfo.external_url}</Link>
                </Grid>
              </Grid>
            </Grid>
            }
            <Grid container spacing={1} className={styles.gridPerfil}>
              { this.state.result.media.map((data,index) => (
              <Grid item xs={6} sm={4} md={4} key={data[index].id || data[index].displayImage} className={styles.discribePerfil}>
                <Grid item xs={6} sm={4} md={4}>
                  <ButtonBase className={styles.buttonBaseFeed}
                    href={data[index].postLink }
                  >
                    <img
                      src={data[index].displayImage}
                      alt={data[index].accessibilityCaption || 'Instagram picture'}
                      className={styles.image}
                    />
                  </ButtonBase>
                </Grid>
              </Grid>
            // {status === 'loading' && <p>loading...</p>}
            // {status === 'failed' && <p>Check instagram here</p>}
            ))}
          </Grid>
          <Grid container spacing={1} className={styles.textBottonInstagram}>
            <Grid item xs={12} sm={12} md={12} className={styles.textPerfilInstagram}>
              <Link className={styles.textColorBlue} href={'https://www.instagram.com/'+ this.state.result.accountInfo.username}> Ir para perfil do instagram</Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      </div>
    );
  }
}



