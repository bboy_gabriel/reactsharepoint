import { LinkedinProps } from './LinkedinProps';
import Axios from 'axios';
import {get, slice} from 'lodash';
// import slice from 'lodash/slice';

/**
 * This function returns a promise that when resolves return data extracted from the public profile page of an linkedin account.
 */
async function getLinkedinFeedInfo(
  account: string,
  options: {
    numberOfMediaElements: number,
    discardVideos: boolean,
  } = {
    numberOfMediaElements: 12,
    discardVideos: false,
  },
): Promise<LinkedinProps> {
  let media: any[];
  let accountInfo: any; 

    // [{
    //   id: any,
    //   displayImage: any,
    //   thumbnail: any,
    //   likes: any,
    //   caption: any,
    //   commentsNumber: any,
    //   accessibilityCaption: any,
    //   dimensions: { width: any, height: any },
    //   postLink: any,
    // }]; 

  try {
    const userInfoSource = await Axios.get(
      `https://www.instagram.com/${account}/`,
    );
    // userInfoSource.data contains the HTML from Axios
    const jsonObject = userInfoSource.data
      .match(
        /<script type="text\/javascript">window\._sharedData = (.*)<\/script>/,
      )[1]
      .slice(0, -1);


    accountInfo = JSON.parse(jsonObject).entry_data.ProfilePage[0].graphql.user;
    // .entry_data.ProfilePage[0].graphql.user;

    // Retrieve media info
    let mediaArray:any;
    mediaArray = slice(
      JSON.parse(jsonObject).entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges,
      0,
      options.numberOfMediaElements,
    );

    // let result : [{
    //     id: string,
    //     displayImage: string,
    //     thumbnail: string,
    //     likes: number,
    //     caption: string,
    //     commentsNumber: number,
    //     accessibilityCaption: string,
    //     dimensions: { width: number, height: number },
    //     postLink: string,
    //   }] ;
    // [{
    //   id: string,
    //   displayImage: string,
    //   thumbnail: string,
    //   likes: number,
    //   caption: string,
    //   commentsNumber: number,
    //   accessibilityCaption: string,
    //   dimensions: { width: number, height: number },
    //   postLink: string,
    // }];

    media = await mediaArray.map(function(value,index, result){
      // Process only if is an image
      result[index] = {
        id: get(value, 'node.id'),
        displayImage: get(value, 'node.display_url'),
        thumbnail: get(value, 'node.thumbnail_src'),
        likes: get(value, 'node.edge_liked_by.count'),
        caption: get(value, 'node.edge_media_to_caption.edges[0].node.text'),
        commentsNumber: get(value, 'node.edge_media_to_comment.count'),
        accessibilityCaption: get(value, 'node.accessibility_caption'),
        dimensions: get(value, 'node.dimensions'),
        postLink: `https://www.instagram.com/p/${get(value, 'node.shortcode')}/`,
      }
      // result.push({
      //   id: get(value, 'node.id'),
      //   displayImage: get(value, 'node.display_url'),
      //   thumbnail: get(value, 'node.thumbnail_src'),
      //   likes: get(value, 'node.edge_liked_by.count'),
      //   caption: get(value, 'node.edge_media_to_caption.edges[0].node.text'),
      //   commentsNumber: get(value, 'node.edge_media_to_comment.count'),
      //   accessibilityCaption: get(value, 'node.accessibility_caption'),
      //   dimensions: get(value, 'node.dimensions'),
      //   postLink: `https://www.instagram.com/p/${get(value, 'node.shortcode')}/`,
      // });
      return result;
    })

  } catch (e) {
    throw new Error(`Unable to retrieve info. Reason: ${e.toString()}`);
  }

  console.log(media);
  console.log(accountInfo);
  
  return await {
    "accountInfo": accountInfo,
    "accountFollowedBy": get(accountInfo, 'edge_followed_by.count'),
    "accountFollow": get(accountInfo, 'edge_follow.count'),
    "postsCount": get(accountInfo, 'edge_owner_to_timeline_media.count'),
    "profilePic": get(accountInfo, 'profile_pic_url_hd'),
    "accountName": get(accountInfo, 'username'),
    "media": [media[0]],
  };
}

export default getLinkedinFeedInfo;
