import * as React from 'react';
import styles from './Linkedin.module.scss';
import { LinkedinProps } from './LinkedinProps';
import Grid from '@material-ui/core/Grid';
import ButtonBase from '@material-ui/core/ButtonBase';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { CommandBarButton, IIconProps, Stack, IStackStyles } from 'office-ui-fabric-react';
import getLinkedinFeedInfo from './LinkedinSevice';
import { any } from 'prop-types';

export default class Linkedin extends React.Component<LinkedinProps, LinkedinProps> {

  constructor(props) {
    super(props);
    this.state = {
      result: {account: any,
        accountInfo: any,
        accountFollowedBy: any,
        accountFollow: any,
        postsCount: any,
        profilePic: any,
        accountName: any,
        media: [],},
      account: any,
      accountInfo: any,
      accountFollowedBy: any,
      accountFollow: any,
      postsCount: any,
      profilePic: any,
      accountName: any,
      media: [],
    }
  }

  async componentDidMount() {
    await getLinkedinFeedInfo(this.props.accountNameLinkedin)
      .then(result => {
        console.log(result);

        // Will send this state as props to the component
        this.setState({ result })
        console.log(this.state);
        
      })
      .catch((e) => {
        console.log('error',e);
        // updateStatus('failed');
      });
  }

  // async componentDidUpdate() {
  //   await getLinkedinFeedInfo(this.props.accountNameLinkedin)
  //     .then(result => {
  //       console.log(result);

  //       // Will send this state as props to the component
  //       this.setState({ result })
  //       console.log(this.state);
        
  //     })
  //     .catch((e) => {
  //       console.log('error',e);
  //       // updateStatus('failed');
  //     });
  // }

  private goTo(url) {
    const link = document.createElement('a');
    link.target = '';
    link.href = url;
    document.body.appendChild(link);
    link.click();
  }

  public render(): React.ReactElement {

    // const menuProps: IContextualMenuProps = {
    //   items: [
    //     {
    //       key: 'emailMessage',
    //       text: 'Email message',
    //       iconProps: { iconName: 'Mail' }
    //     },
    //     {
    //       key: 'calendarEvent',
    //       text: 'Calendar event',
    //       iconProps: { iconName: 'Calendar' }
    //     }
    //   ]
    // };
    const likeIcon: IIconProps = { iconName: 'Like' };
    const commentIcon: IIconProps = { iconName: 'CommentAdd' };
    const shareIcon: IIconProps = { iconName: 'Share' };
    const stackStyles: Partial<IStackStyles> = { root: { height: 30 } };

    return (
      <div>
        <Grid container spacing={1} className={styles.colorBackGroundBlue}>
          <Grid container spacing={1} className={styles.textLinkedinHeader}>
            <Grid item xs={2} sm={1} md={1} className={styles.gridSvgLinkedin}>
              <div className={styles.svgLinkedin}></div>
            </Grid>
            <Grid item xs={10} sm={10} md={10} className={styles.gridTextHeaderLinkedin}>
              <Grid item xs={12} sm={12} md={12} >
                <div className={styles.marginBottom10}>Linkedin</div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} >
                <div className={styles.barraPerfilLinkedin}></div>
              </Grid>
            </Grid>
          </Grid>
          <Grid container spacing={1} className={styles.gridPerfil}>
            { (!this.state.result.accountInfo.username) ? 'Usuario não encontrado' : 
            <Grid container spacing={1} className={styles.gridPerfil}>
              <Grid item xs={12} sm={3} md={3} >
                <ButtonBase className={styles.buttonBase}
                  href={this.state.account.external_url }>
                  <img
                    src={this.state.result.accountInfo.profile_pic_url}
                    alt={this.state.result.accountInfo.profile_pic_url || 'Linkedin picture'}
                    className={styles.imagePerfil}/>
                  </ButtonBase>
              </Grid>
              <Grid item xs={12} sm={9} md={9} className={styles.discribePerfil}>
                <Grid item xs={12} sm={10} md={12}>
                  <span>
                    <div  className={styles.subscribeButton} onClick={() => { this.goTo('https://www.linkedin.com/'+ this.state.result.accountInfo.username); }}>Seguir</div>
                    {this.state.result.accountInfo.full_name}
                  </span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span className={styles.paddingRight}>{this.state.result.postsCount +' publicações'} </span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span>{this.state.result.accountInfo.biography} </span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <span>Estágio: Link abaixo</span>
                </Grid>
                <Grid item xs={12} sm={10} md={12}>
                  <Link className={styles.textColorBlue} href={this.state.result.accountInfo.external_url}> {this.state.result.accountInfo.external_url}</Link>
                </Grid>
              </Grid>
            </Grid>
            }
            <Grid container spacing={1} className={styles.gridPerfilPost}>
              
              <Grid container spacing={1} className={styles.gridPerfil}>
                <Grid item xs={12} sm={3} md={3} >
                  <ButtonBase className={styles.buttonBase}
                    href={this.state.account.external_url }>
                    <img
                      src={this.state.result.accountInfo.profile_pic_url}
                      alt={this.state.result.accountInfo.profile_pic_url || 'Linkedin picture'}
                      className={styles.imagePerfil}/>
                    </ButtonBase>
                </Grid>
                <Grid item xs={12} sm={9} md={9} className={styles.discribePerfil}>
                  <Grid item xs={12} sm={10} md={12}>
                    <span>{this.state.result.accountInfo.biography} </span>
                  </Grid>
                  <Grid item xs={12} sm={10} md={12}>
                    <span>seguidores: {this.state.result.accountFollowedBy}</span>
                  </Grid>
                </Grid>
              </Grid>

              { this.state.result.media.map((data,index) => (
              <Grid item xs={12} sm={12} md={12} key={data[index].id || data[index].displayImage} className={styles.discribePerfilPosts}>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid item xs={12} sm={12} md={12}>
                    <span>{data[index].caption}</span>
                  </Grid>
                  <ButtonBase className={styles.buttonBaseFeed}
                    href={data[index].postLink }
                  >
                    <img
                      src={data[index].displayImage}
                      alt={data[index].accessibilityCaption || 'Linkedin picture'}
                      className={styles.image}
                    />
                  </ButtonBase>
                  <Grid item xs={12} sm={12} md={12}>
                    <span>{data[index].commentsNumber} comentários</span>
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                  <Stack horizontal styles={stackStyles}>
                    <CommandBarButton
                      iconProps={likeIcon}
                      text="Gostei"
                      // Set split=true to render a SplitButton instead of a regular button with a menu
                      // split={true}
                      // menuProps={menuProps}
                    />
                    <CommandBarButton iconProps={commentIcon} text="Comentar" />
                    <CommandBarButton iconProps={shareIcon} text="Compartilhar" />
                  </Stack>
                  </Grid>
                </Grid>
              </Grid>
            // {status === 'loading' && <p>loading...</p>}
            // {status === 'failed' && <p>Check linkedin here</p>}
            ))}
          </Grid>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} className={styles.textPerfilLinkedin}>
              <Link className={styles.textColorBlue} href={'https://www.linkedin.com/'+ this.state.result.accountInfo.username}> Ir para perfil do linkedin</Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      </div>
    );
  }
}



