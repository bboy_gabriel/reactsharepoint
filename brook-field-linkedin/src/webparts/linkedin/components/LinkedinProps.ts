export interface LinkedinProps {
  result?:any;
  account?: any;
  accountNameLinkedin?: any,
  accountInfo: any,
  accountFollowedBy: any,
  accountFollow: any,
  postsCount: any,
  profilePic: any,
  accountName: any,
  media: Array<{
    id: string,
    displayImage: string,
    thumbnail: string,
    likes: number,
    caption: string,
    commentsNumber: number,
    accessibilityCaption: string,
    dimensions: { width: number, height: number },
    postLink: string,
  }>,
}
