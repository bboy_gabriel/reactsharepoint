declare interface ILinkedinWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AccontNameFieldLabel: string;
  AccontQntFieldLabel: string;
}

declare module 'LinkedinWebPartStrings' {
  const strings: ILinkedinWebPartStrings;
  export = strings;
}
