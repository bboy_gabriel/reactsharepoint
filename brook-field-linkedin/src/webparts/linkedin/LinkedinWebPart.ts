import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
} from '@microsoft/sp-webpart-base';

import Linkedin from './components/Linkedin';

// import styles from './LinkedinWebPart.module.scss';
import * as strings from 'LinkedinWebPartStrings';

export interface ILinkedinWebPartProps {
  description: string;
  accountNameLinkedin: string;
}

export default class LinkedinWebPart extends BaseClientSideWebPart<ILinkedinWebPartProps> {

  public render(): void {
    console.log(this);
    
    const element: React.ReactElement<ILinkedinWebPartProps> = React.createElement(
      Linkedin,
      {
        context: this.context,
        description: this.properties.description,
        accountNameLinkedin: this.properties.accountNameLinkedin,
        siteUrl: this.context.pageContext.web.absoluteUrl,
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected get disableReactivePropertyChanges(): boolean {
    // require an apply button on the property pane
    return true;
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {

    const {
      description,
      accountNameLinkedin,
    } = this.properties;

    return {
      pages: [
        {
          displayGroupsAsAccordion:true,
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel,
                  value: description
                }),
                PropertyPaneTextField('accountNameLinkedin', {
                  label: strings.AccontNameFieldLabel,
                  value: accountNameLinkedin
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
